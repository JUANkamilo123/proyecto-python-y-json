from accounts.login_user import *
from usuario.all_item.items import *
from accounts.create_account import *
from usuario.all_tag.tags import *


def main_Page(user):
    '''
    Menu del usuario despues de ingresar
    correctamente usuario y contraseña
    '''
    while (True):
        print("----------------")
        print("User menu Items")
        print("----------------")
        print("1. Items")
        print("2. Tags")
        print("3. Sign off")

        while True:
            option = input("Enter your Option: ")
            if option != "":
                break

        if option == "1":
            main(user) #Main Principal de Item
        elif option == "2":
            main_tags(user)
        elif option == "3":
            break

