import json
from usuario.user_options import *
from usuario.all_item.items import *


tag = {}

def add_tag(user):
    '''
    Funcion de creacion de tag pidiendo nombre tag , chekeo tag y citando nombre usuario

    '''
    tag = {}
    tag["Tag"] = input("Tag: ")
    tag_asignar = tag["Tag"]
    check = check_tag(user,tag_asignar)
    if check == True:
        print("Error. Tag already registered: ")
        print("================================")
    elif check == False:
        print("Registering Tag ...")
        tag["Type"] = "Tag"
        tag["User"] = user
        tag["Id_Item"]=[]
        archivo = lista_completa()
        archivo.append(tag)
        with open("usuario/list.json", "w") as f:
            f.write(json.dumps(archivo))
        print("================================")


def check_tag(user,tag_asignar):
    '''
    Funcion de chequeo un  tag a un item
    '''
    datos = leer_archivo()
    for tag in datos:
        if tag["User"] == user:
            if tag["Type"] == "Tag":
                if tag["Tag"] == tag_asignar:
                    return True
    return False

def view_tags(user):

    '''
    Funcion de la visualisacion de tags que tiene usuario  citando usuario del json y trayendo tag que tiene
    esa id de usuario
    '''
    print("----------------------------------------------------------------")
    print("Tag")
    print("----------------------------------------------------------------")
    archivo = lista_completa()
    for tag in archivo:
        if tag["User"] == user:
            if tag["Type"] == "Tag":
                print(tag["Tag"])

def search_tag(user):

    '''
    Funcion de busqueda de tag que por medio palabras va json con la id usuario y chequea si el tag existe o no
    
    '''
    view_tags(user)
    tag_busqueda = input("Enter the Tag: ")
    archivo = leer_archivo()
    for tag in archivo:
        if tag["User"] == user:
            if tag["Type"] == "Tag":
                if tag["Tag"] == tag_busqueda:
                    print("----------------")
                    print("Tag Found")
                    print("----------------")
                    print("Tag: " + tag["Tag"])
                    print("--------------------------------------------------------")
                    print("Id", " - ", "Title", " - ", "Username", " - ", "URL")
                    print("--------------------------------------------------------")

                    datos = lista_completa()
                    for item in datos:
                        if item["User"] == user:
                            if item["Type"] == "Item":
                                id_item_tag = tag["Id_Item"]
                                for x in range(0,len(id_item_tag)):

                                    items_encontrados = []
                                    if id_item_tag[x] == item["Security"]:
                                        items_encontrados.append(item["Title"])

                                        print(item["Id"], " - ", item["Title"], " - ",item["Username"], " - ",item["URL"])

def edit_tag(tag_edit,user):
    '''
    Funcion de editar tag se desplega menu y se muestra opciones dependiendo opciones 1 editar tag 
    si el tag no existe da un error 
    '''

    while True:
        print("----------------")
        print("All your Tags Created: ")
        print("----------------")
        print("Select an Option: ")
        print("1. Edit Tag")
        print("2. Back to main menu")
        datos = leer_archivo()
        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            check = check_tag(user,tag_edit)
            if check == True:
                print("Tag found: ")
                for tag in datos:
                    if tag["User"] == user:
                        if tag["Type"] == "Tag":
                            if tag["Tag"] == tag_edit:
                                edit = input("New Tag: ")
                                tag["Tag"] = edit
                                escribir_archivo(datos)
                                view_tags(user)
                print("================================")
            elif check == False:
                print("Error. Tag not registered")
                print("================================")
        elif op == "2":
            break

def delete_tag(tag_delete,user):
        '''
        
        '''
        print("----------------")
        print("Delate Tag: ")
        print("----------------")
        print("Select an Option: ")
        print("1. Confirm")
        print("2. Cancel ")
        datos = leer_archivo()
        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            for tag in datos:
                if tag["User"] == user:
                    if tag["Type"] == "Tag":
                        if tag["Tag"] == tag_delete:
                            tag.clear()
                            datos = [elemento for elemento in datos if elemento]
                            escribir_archivo(datos)
                            print("Tag Removed")
        elif op == "2":
            main_tags(user)

def main_tags(user):
    while True:
        print("----------------")
        print("Tags Menu")
        print("----------------")
        print("1. Add Tag")
        print("2. View Tags")
        print("3. Search Tags")
        print("4. Edit Tag")
        print("5. Delete Tag")
        print("6. Return to Main Menu")

        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            add_tag(user)
        elif op == "2":
            view_tags(user)
        elif op == "3":
            search_tag(user)
        elif op == "4":
            view_tags(user)
            tag_edit = input("Enter Tag to Edit: ")
            edit_tag(tag_edit,user)
        elif op == "5":
            tag_delete = input("Enter Tag to Delete: ")
            check = check_tag(user,tag_delete)
            if check == True:
                print("Tag found: ")
                delete_tag(tag_delete,user)
                print("================================")
            elif check == False:
                print("Error. Tag not registered")
                print("================================")
        elif op == "6":
            break