import json
import uuid
from usuario.user_options import *
from accounts.encry_decry import *



lista_items = list()
item = {}
lista_final = list()


def crear_item(user):
    '''
    Funcion que permite la creación de un Item con los parametros definidos.
    '''

    security = str(uuid.uuid4())[:3]
    item = {}
    item["Security"] = security
    item["Type"] = "Item"
    item["User"] = user
    item["Title"] = input("Title: ")
    while (len(item["Title"])) == 0:
        print("Title field Empty")
        item["Title"] = input("Title: ")
    item["Username"] = input("Username: ")

    while (len(item["Username"])) == 0:
        print("Username field Empty")
        item["Username"] = input("Username: ")

    password = input("Password: ")
    while len(password) == 0:
        print("Password field Empty")
        password = input("Password: ")
    pass_encrypt = encrypt(password)
    item["Password"] = pass_encrypt

    item["URL"] = input("URL: ")
    while (len(item["URL"])) == 0:
        print("URL field Empty")
        item["URL"] = input("URL: ")

    item["Commentary"] = input("Commentary: ")
    archivo = lista_completa()
    archivo.append(item)
    with open("usuario/list.json", "w") as f:
        f.write(json.dumps(archivo))

    while True:
        print("Add Item to Tag?")
        print("--------------------------------")
        print("Select an Option: ")
        print("1. Add Item to Tag: ")
        print("2. Exit: ")
        datos = leer_archivo()
        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            tag_asignable = input("Enter Tag name: ")
            # check = verify_tag(tag_asignable, user,security)
            check = verify_item_tag(user,tag_asignable,security)
            if check == True:
                print("Error. Registered Id Tag")
                print("================================")
            elif check == False:
                for tag in datos:
                    if tag["User"] == user:
                        if tag["Type"] == "Tag":
                            if tag["Tag"] == tag_asignable:
                                tag["Id_Item"].append(security)
                                escribir_archivo(datos)
                print("================================")
        elif op == "2":
            break

    option = input(
        "View all Created Items?. (y) to confirm, or Press any key to return: ")
    if option == "y":
        view_items(user)
    else:
        main(user)


def view_items(user):
    print("----------------------------------------------------------------")
    print("Id", " - ", "Title", " - ", "URL", " - ", "Commentary")
    print("----------------------------------------------------------------")
    id(user)


def id(user):
    archivo = lista_completa()
    Id = 0
    for item in archivo:
        if item["User"] == user:
            if item["Type"] == "Item":
                item["Id"] = Id
                Id += 1

    with open("usuario/list.json", "w") as f:
        f.write(json.dumps(archivo))

    for item in archivo:
        if item["User"] == user:
            if item["Type"] == "Item":
                print(item["Id"], " - ", item["Title"], " - ", item["URL"], " - ", item["Commentary"])


def search_item(id,user):
    datos = leer_archivo()
    for item in datos:
        if item["User"] == user:
            if item["Type"] == "Item":
                if item["Id"] == id:
                    pass_encrypt = item["Password"]
                    desencriptado = decrypt(pass_encrypt)
                    print(
                        "----------------------------------------------------------------")
                    print("Id", " - ", "Title", " - ", "Username", " - ", "Password", " - ", "URL", " - ", "Commentary")
                    print(
                        "----------------------------------------------------------------")
                    print(item["Id"], " - ", item["Title"], " - ", item["Username"],
                          " - ", desencriptado, " - ", item["URL"], " - ", item["Commentary"])
                    print(
                        "----------------------------------------------------------------")
                    print("Tag List")
                    print(
                        "----------------------------------------------------------------")
                    Id_security = item["Security"]
                    archivo = lista_completa()
                    for tag in archivo:
                        if tag["User"] == user:

                            if tag["Type"] == "Tag":

                                lista = tag["Id_Item"]

                                for x in range(0, len(lista)):

                                    fuentes = []
                                    if lista[x] == Id_security:
                                        fuentes.append(tag["Tag"])
                                        print(fuentes)


def delete_item(id, user):
        print("----------------")
        print("Delate Item: ")
        print("----------------")
        print("Select an Option: ")
        print("1. Confirm")
        print("2. Cancel ")
        datos = leer_archivo()
        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            del datos[id]
            escribir_archivo(datos)
        elif op == "2":
            main(user)


def view_items_all(user):
    print("----------------------------------------------------------------")
    print("Id", " - ", "Title", " - ", "Username", " - ",
          "Password", " - ", "URL", " - ", "Commentary")
    print("----------------------------------------------------------------")
    id_all(user)


def id_all(user):
    archivo = lista_completa()
    Id = 0
    for item in archivo:
        if item["User"] == user:
            if item["Type"] == "Item":
                item["Id"] = Id
                Id += 1


    with open("usuario/list.json", "w") as f:
        f.write(json.dumps(archivo))

    for item in archivo:
        if item["User"] == user:
            if item["Type"] == "Item":
                print(item["Id"], " - ", item["Title"], " - ", item["Username"], " - ",
                      item["Password"], " - ", item["URL"], " - ", item["Commentary"])


def lista_completa():
    with open("usuario/list.json") as f:
        archivo = json.loads(f.read())
        return archivo


def leer_archivo():
    '''
    Lee la información de un archivo JSON y lo carga en un diccionario y lo retorna
    '''
    # Abre el archivo en modo lectura
    archivo = open("usuario/list.json", "r")
    # Carga la información en un diccionario
    datos = json.load(archivo)
    # Cierra el archivo
    archivo.close()
    # Retorna el diccionario
    return datos


def escribir_archivo(datos):
    '''
    Escribe un diccionario en un archivo
    '''
    # Abre el archivo en modo escritura
    archivo = open("usuario/list.json", "w")
    # Guarda el diccionario en el archivo
    json.dump(datos, archivo)
    # Cierra el archivo
    archivo.close()


def edit_item(id,user):
    while True:
        print("----------------")
        print("All your Items Created: ")
        print("----------------")
        print("Select an Option: ")
        print("1. Edit Title")
        print("2. Edit Username ")
        print("3. Edit Password ")
        print("4. Edit URL")
        print("5. Edit Comment")
        print("6. Edit Tag List")
        print("7. Back to main menu")
        datos = leer_archivo()
        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            edit = input("New Title: ")
            for item in datos:
                if item["User"] == user:
                    if item["Type"] == "Item":
                        if item["Id"] == id:
                            item["Title"] = edit
                            escribir_archivo(datos)
                            view_items_all(user)
        elif op == "2":
            edit = input("New Username: ")
            for item in datos:
                if item["User"] == user:
                    if item["Type"] == "Item":
                        if item["Id"] == id:
                            item["Username"] = edit
                            escribir_archivo(datos)
                            view_items_all(user)
        elif op == "3":
            edit = input("New Password: ")
            for item in datos:
                if item["User"] == user:
                    if item["Type"] == "Item":
                        if item["Id"] == id:
                            item["Password"] = edit
                            escribir_archivo(datos)
                            view_items_all(user)
        elif op == "4":
            edit = input("New URL: ")
            for item in datos:
                if item["User"] == user:
                    if item["Type"] == "Item":
                        if item["Id"] == id:
                            item["URL"] = edit
                            escribir_archivo(datos)
                            view_items_all(user)
        elif op == "5":
            edit = input("New Comment: ")
            for item in datos:
                if item["User"] == user:
                    if item["Type"] == "Item":
                        if item["Id"] == id:
                            item["Commentary"] = edit
                            escribir_archivo(datos)
                            view_items_all(user)
        elif op == "6":
            edit = "Prueba2"
            for item in datos:
                if item["User"] == user:
                    if item["Type"] == "Item":
                        if item["Id"] == id:
                            security = item["Security"]
                            ##Encuentro identificación del Item que se quiere editar en Tags
                            print("--------------------------------")
                            print("Item Tags: ")
                            for tag in datos:
                                if tag["Type"] == "Tag":
                                    if tag["User"] == user:
                                        if security in tag["Id_Item"]:
                                            print(tag["Tag"]) ##Tags asociados al Item
                            # sele = input("Tag a Editar: ")
                            ##Preguntar si se desea borrar uno de los Tags asociados al Item, o asignar el Item a un nuevo Tag
                            while True:
                                print("--------------------------------")
                                print("Select an Option: ")
                                print("1. Add Tag: ")
                                print("2. Delete Tag: ")
                                print("3. Exit: ")
                                while True:
                                    op = input("Enter your Option: ")
                                    if op != "":
                                        break
                                if op == "1":
                                    tag_asignable = input("Ingrese nombre del Tag: ")
                                    check = verify_item_tag(user,tag_asignable,security)
                                    if check == True:
                                        print("Error. Registered Id Tag")
                                        print("================================")
                                    elif check == False:
                                        print("================================")
                                        for tag in datos:
                                            if tag["User"] == user:
                                                if tag["Type"] == "Tag":
                                                    if tag["Tag"] == tag_asignable: ## Se comprueba si el tag esta creado
                                                        tag["Id_Item"].append(security)
                                                        escribir_archivo(datos)
                                elif op == "2":
                                    tag_delete = input("Ingrese nombre del Tag: ")
                                    for tag in datos:
                                        if tag["User"] == user:
                                            if tag["Type"] == "Tag":
                                                if tag["Tag"] == tag_delete: ## Se comprueba si el tag esta creado
                                                    lista = tag["Id_Item"]
                                                    for x in range(0,len(lista)):
                                                        if lista[x] == security:
                                                            tag["Id_Item"].pop(x)
                                                            escribir_archivo(datos)
                                                        else:
                                                            "Tag no Creado"
                                elif op == "3":
                                    break
        elif op == "7":
            break


def verify_item_tag(user,tag_asignable,security):
    datos = leer_archivo()
    for tag in datos:
        if tag["User"] == user:
            if tag["Type"] == "Tag":
                if tag["Tag"] == tag_asignable:
                    lista = tag["Id_Item"]
                    for x in range(0,len(lista)):
                        if lista[x] == security:
                            return True
    return False


def main(user):
    while True:
        print("----------------")
        print("Items Menu")
        print("----------------")
        print("1. Add Item")
        print("2. Edit Items")
        print("3. Delete Items")
        print("4. View Items")
        print("5. Search Item")
        print("6. Return to Main Menu")

        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            crear_item(user)
        elif op == "2":
            view_items(user)
            id = int(input("Enter Id of the Item to Edit: "))
            check = verify_id(user,id)
            if check == True:
                print("Id found: ")
                print("================================")
                edit_item(id, user)
            elif check == False:
                print("Error. Id not registered")
                print("================================")
        elif op == "3":
            view_items_all(user)
            id = int(input("Enter Id of the Item to Delete: "))
            check = verify_id(user,id)
            if check == True:
                print("Id found: ")
                delete_item(id, user)
                print("================================")
            elif check == False:
                print("Error. Id not registered")
                print("================================")
        elif op == "4":
            view_items(user)
        elif op == "5":
            id = int(input("Enter Id of the Item to Search: "))
            check = verify_id(user,id)
            if check == True:
                print("Id found: ")
                search_item(id, user)
                print("================================")
            elif check == False:
                print("Error. Id not registered")
                print("================================")
        elif op == "6":
            break


def verify_id(user,id):
    datos = leer_archivo()
    for item in datos:
        if item["User"] == user:
            if item["Type"] == "Item":
                if item["Id"] == id:
                    return True
    return False