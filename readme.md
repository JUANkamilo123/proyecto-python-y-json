# Repositorio de Aplicaciones de Software para Telcomunicaciones 20211

_Estudiantes:_
- Juan Camilo Duran Rincón
- Alejandro Cortazar Guillen

## Descripción

Proyecto software que permite a un usuario administrar las claves de los diferentes sitios web que visita.

## Herramientas

- VSCode
- Git

## Lenguajes

- Python

## Formato de archivo

- Json