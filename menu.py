import json
from accounts.encry_decry import *
from accounts.create_account import *
from accounts.login_user import *


def menu():
    '''
    Menu Principal de Programa

    '''
    while True:
        print("----------------")
        print('\nPASSWORD REGISTRATION PLATFORM\n')
        print("----------------")
        print('1. Sign Up')
        print('2. Login')
        print('3. Exit')

        while True:
            op = input("Enter your Option: ")
            if op != "":
                break
        if op == "1":
            create_accounts()
        elif op == "2":
            login()
        elif op == "3":
            break


menu()