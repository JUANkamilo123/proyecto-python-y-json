import json
from accounts.create_account import *
from accounts.encry_decry import *
from usuario.user_options import *
from getpass import getpass

logged = False


def login():
    '''
    Funcion que permite relizar el ingreso del usuario a la aplicacion 
    '''
    user = input("User: ")
    password = getpass("password: ")

    if user_authentication(user, password):
        #Funcion que recive un true si la aunteticacion es correcta. se envia a la pagina principal del usuario
        logged = True
        print("Authenticated User")
        main_Page(user)
        #Condicional para en caso que la autenticacion es erronea se envia un mensaje para crear una cuenta.
    else:
        print("\nYour account isn't registered. " "¿Create account? y/n " "\npress any key to return to the main menu")
        option = input("")
        if option == "y":
            create_accounts()
        elif option == "n":
            print("Hola")
            login()
        else:
            print("Returning to main menu")


def user_authentication(user, password): #Función que verifica si password y user son los que se registraron. # Permite realizar el Login
    account = accounts()
    for usuario in account:
        if (usuario['user'] == user):
            if (usuario['password'] == encrypt(password)):
                return True
