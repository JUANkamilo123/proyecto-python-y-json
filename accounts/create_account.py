import json
from accounts.encry_decry import *
from getpass import getpass

admin = False


def accounts(): #Funcion que lee Json de los Registros de Usuario 
    with open("accounts/user_accounts.json") as f:
        Users = json.loads(f.read())
        return Users


def user_existence(user): #Función que verifica la existencia de un user
    datos = accounts()
    for usuario in datos: #For para buscar dentro del Json la existencia del usuario que se quiere crear.
        if usuario["user"] == user: #Se busca SI el usuario a registrar ya esta registrado
            print("User can't be created because it's already registered")
            return True #Retorna True SI existe un usuario registrado con el USER que el ANONIMO quiere registrar
    return False #Retorna False SI NO existe un usuario registrado con el USER que el ANONIMO quiere registrar


def create_accounts():
    '''
    Funcion para Crear una nueva cuenta de usuario
    '''
    datos = accounts()#Se lee archivo json de las cuentas creadas
    #ingreso de datos 
    user = input("Enter your User: ")
    checks = user_existence(user) #Se envia user ingresado a la función
    #while que verifica que no se ingreso ningun dato y vuelve pedir el dato.
    while len(user) == 0:
        print("User field Empty")
        user = input("Enter your User: ")
    while checks == True:
        user = input("Enter your User: ")
        checks = user_existence(user)
    if checks == False:
        password = getpass("Enter your Password: ")
        while len(password) == 0:
            print("Password field Empty")
            password = getpass("Enter your Password: ")
        name = input("Enter your Name: ")
        while len(name) == 0:
            print("Name field Empty")
            name = input("Enter your Name: ")

    password_encrypt = encrypt(password)#Funcion Que Permite Encriptar La Contraseña Del Usuario.
    password_decrypt = decrypt(password_encrypt)
    #Creacion del diccionario 
    create_account = {}
    #Ingreso de clave valor en el diccionario
    create_account["user"] = user
    create_account["password"] = password_encrypt
    create_account["name"] = name
    #metodo que permite almacenar el diccionario dentro de "datos"
    datos.append(create_account)
    #se abre la ubicacion de json como modo escritura como archivo
    with open("accounts/user_accounts.json", "w") as f:
        f.write(json.dumps(datos))

    # print(create_account)
    print("\nSUCCESSFUL USER REGISTRATION")
    print("RETURNING TO THE HOME MENU\n")
    print("--------------------------------")
    return create_account
