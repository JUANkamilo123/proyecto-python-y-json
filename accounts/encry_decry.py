'''
Funciones que permiten encriptar las contraseñas de los usuarios, alterando los caracteres.
'''
def encrypt(message):
    newS = ""
    for car in message:
        newS = newS + chr(ord(car) + 2)
    return newS


# print(encrypt('hello world'))


def decrypt(message):
    newS = ""
    for car in message:
        newS = newS + chr(ord(car) - 2)
    return newS


# print(decrypt('jgnnq"yqtnf'))
